var Carts = function() {
    if ($('#toggle-donate').is(':checked')) {
        var ticketValue = Number($(".total-item").val()) ;
        var totalTicket = Number($(".total-tickets").val()) ;
        if(ticketValue != totalTicket)
        {
            $(".total-tickets").val(ticketValue * 2)
        }
        $.ajax({
            url: `${config.base_url}/update-number-coupon`,
            type: "PATCH",
            headers: {
                "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
            },
           /* data: {
                cart_id: selector.find(".cart_id").val(),
                quantity: selector.find(".qty").val()
            },*/
            success: function (result) {
                console.log(result)
            }
        });

    }
    else
    {
        var ticketValue = parseInt(Number($(".total-item").val() ) );
        $(".total-tickets").val(ticketValue)
    }
    var handleQuantity = function () {
        $(document).on("click", ".cart-qty-event", function (e) {
            e.preventDefault();
            let selector = $(this).closest("li");
            let operator = $(this).val();
            let current_quantity = 0;
            if(operator === "+") {
                current_quantity = parseInt(selector.find(".qty").val()) + 1;
            } else {
                current_quantity = parseInt(selector.find(".qty").val()) - 1;
            }
            if(current_quantity > 0) {
                selector.find(".qty").val(current_quantity);
                let product_total = parseFloat(selector.find(".unitPrice").val()) * current_quantity;
                selector.find(".sub-total").val(product_total);
                calculateTotal();
                saveOrder(selector);

            }
        });
    };

    // var handleQuantityMinus = function () {
    //     $(document).on("click", ".cart-qty-minus", function (e) {
    //         e.preventDefault();
    //         let selector = $(this).closest("li");
    //         let current_quantity = parseInt(selector.find(".qty").val()) - 1;
    //         if(current_quantity > 0) {
    //             selector.find(".qty").val(current_quantity);
    //             let product_total = parseFloat(selector.find(".unitPrice").val()) * current_quantity;
    //             selector.find(".sub-total").val(product_total);
    //             calculateTotal();
    //         }
    //     });
    // };
    var calculateTotal = function() {
        let total_quantity = 0;
        $(".qty").each(function() {
            total_quantity += parseInt($(this).val());
        });
        $(".total-item").val(total_quantity);

        if ($('#toggle-donate').is(':checked')) {
            $(".total-tickets").val(Number(total_quantity )* 2 );
        }
        else
        {
            $(".total-tickets").val(total_quantity);
        }


        let sub_total = 0;
        $(".sub-total").each(function () {
            sub_total += parseFloat($(this).val());
        });
        $(".net-amount").val(sub_total);
    };

    var saveOrder = function(selector) {
        $.ajax({
            url: `${config.base_url}/update-order-quantity`,
            type: "PATCH",
            headers: {
                "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
            },
            data: {
                cart_id: selector.find(".cart_id").val(),
                quantity: selector.find(".qty").val()
            },
            success: function (result) {
                console.log(result)
            }
        });
    };



    return {
        init: function () {
            handleQuantity();
        }
    }

}();

$(document).ready(function() {
    Carts.init();
});