<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cros'], 'namespace' => 'Api'], function () {

    Route::post('/auth/authenticate', 'AuthController@authenticate');
});

Route::middleware( ['jwt.auth','auth:api', 'jwt.refresh'])->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => ['cros'], 'namespace' => 'Api'], function () {

               // Routing Customers
    Route::post('/user/register', 'UserController@register');
    Route::post('user/login', 'UserController@login');
              // Routing Carts
    Route::post('cart/store', 'CartsController@store');
               // Routing Products
    Route::post('/product/store', 'ProductsController@store');
               // Routing Checkout
    Route::post('/checkout', 'CheckoutController@checkout');

               // Routing Checkout
    Route::post('/order/updateStatus', 'OrdersController@updateOrderStatus');


});
