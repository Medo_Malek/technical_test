<?php

namespace App\models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";
    protected $fillable = ["user_id" , "order_status_id","coupon_code","payment_method"];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orderStatus()
    {
        return $this->belongsTo(OrderStatus::class, 'order_status_id');
    }

    public function Products()
    {
        return $this->belongsToMany(Product::class,'order_products')
            ->withPivot(['quantity','order_id',"product_id"]);
    }

}
