<?php

namespace App\models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table = "order_products";
    protected $fillable = ['quantity','order_id','product_id'];


}
