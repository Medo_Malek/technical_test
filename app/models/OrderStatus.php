<?php

namespace App\models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $table = "order_status";
    protected $fillable = ["name"];


}
