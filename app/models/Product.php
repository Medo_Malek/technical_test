<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{


    protected $table = "products";
    protected $fillable = ["title", "price", "discount", "final_price"];



}
