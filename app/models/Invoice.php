<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Invoice extends Model
{


    protected $table = "invoices";
    protected $fillable = ["total", "sub_total", "shipping_cost", "tax","invoice_status" , "order_id","payment_date"];

    public function order(){
        return $this->belongsTo(Order::class,'order_id');
    }

}
