<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\orders\UpdateOrderRequest;
use App\Http\Requests\Api\products\CreateProductRequest;
use App\Http\Resources\allProductResource;
use App\Http\Resources\ProductDetailResource;
use App\Http\Resources\ProductResource;
use App\models\Order;
use App\models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class OrdersController extends Controller
{

    /**
     * @SWG\Post(
     *      path="/order/updateStatus",
     *      operationId="updateStatus",
     *      tags={"Orders"},
     *      summary="updateStatus",
     *      description="Returns Order Data",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="order_id",
     *          description="Order id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="order_status_id",
     *          description="1 => pending, 2 => processing, 3 => shipping, 4 => delivered, 5 => returned",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function updateOrderStatus(UpdateOrderRequest $request)
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();
            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');

            $status = $this->updateStatus($request->order_status_id);
            Order::where("id",$request->order_id)->where("user_id",$user->id)->update(["order_status_id" => $status]);

            return response()->json(['status' => 200, 'msg' => "Order Status Updated Successfully "]);
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }
    }

    public function updateStatus($value)
    {
        if ($value == 1)
            $order_status_id = $value;

        if ($value == 2)
            $order_status_id = $value;

        if ($value == 3)
            $order_status_id = $value;

        if ($value == 4)
            $order_status_id = $value;

        if ($value == 5)
            $order_status_id = $value;


        return (int)$order_status_id;
    }

}
