<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\carts\CreateCartRequest;
use App\Http\Requests\Api\products\CreateProductRequest;
use App\Http\Resources\allProductResource;
use App\Http\Resources\ProductDetailResource;
use App\Http\Resources\ProductResource;
use App\models\Cart;
use App\models\Order;
use App\models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class CartsController extends Controller
{

    /**
     * @SWG\Post(
     *      path="/cart/store",
     *      operationId="add products to cart",
     *      tags={"Carts"},
     *      summary="Add products to cart",
     *      description="store product to my cart",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="product_id",
     *          description="product id",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="quantity",
     *          description="product quantity",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function store(CreateCartRequest $request)
    {
        try {
            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');


            $product = Product::query()->where("id", $request->product_id)->first();
            if (!$product)
                return apiResponse(401, 'This Product Not Exist ... ');

            $cart = Cart::where("user_id", $user->id)->where("product_id", $product->id)->first();

            if ($cart) {
                Cart::where("id", $cart->id)->update([
                    "quantity" => $request->quantity + $cart->quantity,
                ]);
            } else {
                $data = new Cart();
                $data->user_id = $user->id;
                $data->product_id = $request->product_id;
                $data->quantity = $request->quantity;
                $data->save();
            }

            return response()->json(['status' => 200, 'msg' => "Product Added To Cart Successfully ."]);
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }
    }


}
