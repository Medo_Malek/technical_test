<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\Api\users\ChangePasswordRequest;
use App\Http\Requests\Api\users\CreateUserRequest;
use App\Http\Requests\Api\users\ForgetPasswordRequest;
use App\Http\Requests\Api\users\LoginUserRequest;
use App\Traits\ApiResponser;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Alhoqbani\MobilyWs\SMS;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use JWTAuth;

use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Thomaswelton\LaravelGravatar\Facades\Gravatar;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;


class UserController extends Controller
{

    use ApiResponser;

    /**
     * @SWG\Post(
     *      path="/user/register",
     *      operationId="user regestration",
     *      tags={"Customers"},
     *      summary="user regestration",
     *      description="Returns user Data",
     *     @SWG\Parameter(
     *          name="name",
     *          description="User name",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="email",
     *          description="User email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="phone",
     *          description="User mobile_number",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="password",
     *          description="User password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */

    public function register(CreateUserRequest $request)
    {
        $data = $request->all();

        $data['password'] = bcrypt($request->password);
        $user = User::create($data);
        return response()->json(['status' => 200 ,'msg'=>"Success", 'data' => $user]);
    }




    /**
     * @SWG\Post(
     *      path="/user/login",
     *      operationId="user Login",
     *      tags={"Customers"},
     *      summary="user Login",
     *      description="Returns user Data",
     *      @SWG\Parameter(
     *          name="email",
     *          description="Email",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *
     *      @SWG\Parameter(
     *          name="password",
     *          description="User password",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */

    public function login(LoginUserRequest $request)
    {
        $userEmail = User::query()->where("email", $request->email)->first();
        if ($userEmail && \Illuminate\Support\Facades\Hash::check($request->password, $userEmail->password)) {
            $token = JWTAuth::fromUser($userEmail);
            return response()->json(['status' => 200 ,'msg'=>"Success", 'token' => $token]);
        }
        else {
            return apiResponse(404, "We Haven't User With This Credentials");

        }

    }


}
