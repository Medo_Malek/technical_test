<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\products\CreateProductRequest;
use App\Http\Resources\allProductResource;
use App\Http\Resources\ProductDetailResource;
use App\Http\Resources\ProductResource;
use App\models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{

    /**
     * @SWG\Post(
     *      path="/product/store",
     *      operationId="Add New Product",
     *      tags={"Products"},
     *      summary="Add New Product",
     *      description="Returns Product Data",
     *     @SWG\Parameter(
     *          name="title",
     *          description="Product Title",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="price",
     *          description="Product price",
     *          required=true,
     *          type="string",
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="discount",
     *          description="Product Discount",
     *          required=false,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *
     *         @SWG\Schema(
     *              type="object",
     *
     *      @SWG\Property(
     *                  property="title",
     *                  type="string"
     *              ),
     *),
     *
     *       ),
     *
     *
     *      @SWG\Response(response=400, description="Bad request"),
     *
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:projects", "read:projects"}
     *         }
     *     },
     * )
     *
     */
    public function store(CreateProductRequest $request)
    {
        $data = $request->all();

        if (!$request->has("discount"))
            $final_price = (float)$data["price"];

            else
            $final_price = $this->calculateFinalPrice($data["price"], $data["discount"]);

        if ($final_price <= 0)
            return response()->json(['status' => 403, 'msg' => "Discount Value Must Be Less Than Price"]);


        $data['final_price'] = $final_price;
        $product = Product::create($data);
        return response()->json(['status' => 200, 'msg' => "Success", 'data' => $product]);
    }


    public function calculateFinalPrice($price, $discount)
    {
        $price_after_discount = (float)$price * $discount / 100;
        $final_price = (float)$price - $price_after_discount;
        return $final_price;
    }
}
