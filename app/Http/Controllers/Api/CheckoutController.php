<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\carts\CreateCartRequest;
use App\Http\Requests\Api\orders\CreateOrderRequest;
use App\Http\Requests\Api\products\CreateProductRequest;
use App\Http\Resources\allProductResource;
use App\Http\Resources\ProductDetailResource;
use App\Http\Resources\ProductResource;
use App\models\Cart;
use App\models\Invoice;
use App\models\Order;
use App\models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class CheckoutController extends Controller
{

    /**
     * @SWG\Post(
     *      path="/checkout",
     *      operationId="checkout orders",
     *      tags={"Checkout"},
     *      summary="Checkout",
     *      description="checkout",
     *      @SWG\Parameter(
     *          name="authorization",
     *          description="token",
     *          required=true,
     *          type="string",
     *          in="header"
     *      ),
     *    @SWG\Parameter(
     *          name="payment_method",
     *          description="1 Means Cash On Delivery , 2 Means Pay At Store",
     *          required=true,
     *          type="integer",
     *          in="formData"
     *      ),
     *    @SWG\Parameter(
     *          name="coupon_code",
     *          description="Apply Coupon If You have",
     *          required=false,
     *          type="string",
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *      @SWG\Response(response=400, description="Bad request"),
     *      @SWG\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {
     *             "oauth2_security_example": {"write:offer", "read:offer"}
     *         }
     *     },
     * )
     *
     */

    public function checkout(CreateOrderRequest $request)
    {

        try {
            $user = \JWTAuth::parseToken()->authenticate();

            if (!$user)
                return apiResponse(401, 'You Must Login To Complete This Operation');

            if ($user->carts->count() == 0)
                return response()->json(['status' => 401, 'msg' => "Sorry Your Cart Is Empty "]);


            if ($request->has("coupon_code"))
               $order = $this->createOrder($user, $request->payment_method, $request->coupon_code);

            else
               $order = $this->createOrder($user, $request->payment_method, null);


            $invoice = $this->createInvoice($order);

            return response()->json(['status' => 200, 'msg' => "Your Payment Completed Successfully .","data" => $invoice]);
        } catch (TokenExpiredException $e) {
            return apiResponse(505, 'Your session has been expired, please login again');

        }
    }



    public function createOrder($user, $payment_method, $coupon_code)
    {
        $order = Order::create([
            'order_status_id' => 1,                 //status => pending
            'user_id' => $user->id,
            'payment_method' => $payment_method,     // 1 Means Cash On Delivery , 2 Means Pay At Store
            'coupon_code' => $coupon_code,
        ]);
        $userCarts = $user->carts;
        foreach ($userCarts as $product) {
            $data = [
                'quantity' => $product->quantity,
                'order_id' => $order->id,
                'product_id' => $product->product_id,
            ];
            DB::table('order_products')->insert($data);
        }
        return $order;
    }

    public function createInvoice($order)
    {
        $orderProducts = $order->Products;

        $subtotal = 0;
        foreach ($orderProducts as $product) {
            $subtotal += $product->final_price * $product->pivot->quantity;
        }

        $taxValue = $this->calculateTaxValue($subtotal);
        if ($order->payment_method == 1)
            $shippingCost = $this->calculateShipping($subtotal);

        else
            $shippingCost = 0;

        if ( $order->coupon_code === "A3000")
            $value_after_apply_coupon = $this->calculateTotalAfterApplyCoupon($subtotal);

        else
            $value_after_apply_coupon = null;

        $total = round($subtotal + $taxValue + $shippingCost + $value_after_apply_coupon, 1);
        $invoice = Invoice::create([
            'total' => $total,
            'sub_total' => $subtotal,
            'tax' => $taxValue,
            'shipping_cost' => $shippingCost,
            'invoice_status' => "paid",
            'order_id' => $order->id,
            'payment_date' => Carbon::now(),
        ]);
        return $invoice;
    }



    public function calculateTaxValue($sub_total)
    {
        $value_of_tax = round($sub_total * (10 / 100));
        return $value_of_tax;
    }



    public function calculateShipping($sub_total)
    {
        $value_of_shipping = round($sub_total * (5 / 100));
        return $value_of_shipping;
    }



    public function calculateTotalAfterApplyCoupon($sub_total)
    {
        $value_after_apply_coupon = round($sub_total * (10 / 100));
        return $value_after_apply_coupon;
    }


}
