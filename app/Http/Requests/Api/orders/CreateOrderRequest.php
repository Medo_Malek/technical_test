<?php

namespace App\Http\Requests\Api\orders;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_method'=>'required|integer',
            'coupon_code'=>'string',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response(apiResponse(405, $validator->errors()->first())));
    }

}
