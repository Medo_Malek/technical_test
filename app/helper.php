<?php

use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

if (!function_exists('apiResponse')) {
    function apiResponse($status, $msg, $data = null, $force_data = false, $per_page = null)
    {
        if (!$data && !$force_data) {
            return [
                'status' => $status,
                'msg' => $msg,
            ];
        } else {
            return [
                'status' => $status,
                'msg' => $msg,
                'data' => $per_page ? manual_pagination($data, $per_page ?? $per_page) : $data
            ];
        }
    }
};



if (! function_exists('auth')) {
    /**
     * Get the available auth instance.
     *
     * @param  string|null  $guard
     * @return \Illuminate\Contracts\Auth\Factory|\Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    function auth($guard = null)
    {
        if (is_null($guard)) {
            return app(AuthFactory::class);
        }

        return app(AuthFactory::class)->guard($guard);
    }
}


if (!function_exists('manual_pagination')) {
    function manual_pagination($items, $perPage = 5, $page = null)
    {
        $pageName = 'page';
        $page = $page ?: (Paginator::resolveCurrentPage($pageName) ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator(
            $items->forPage($page, $perPage)->values(),
            ceil($items->count() / $perPage),
            $perPage,
            $page,
            [
                'path' => Paginator::resolveCurrentPath(),
                'pageName' => $pageName,
            ]
        );
    }
}




