<?php

use Illuminate\Database\Seeder;
use \App\Models\OrderStatus;


class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order_status = ["Prepending" , "Processing" , "Shipping" ,"Delivered" , "Returned"];
        $counter = 1;
        for ($i = 0; $i < sizeof($order_status); $i++) {
            \Illuminate\Support\Facades\DB::table('order_status')->insert([
                'id' => $counter++,
                'name' => $order_status[$i],
            ]);
        }
    }
}
