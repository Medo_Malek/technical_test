<?php
return [

    'register'=>'Register',
    'log_in'=>'Login',
    'home'=>'Home',
    'currency'=>'Currency',
    'about_company'=>'About Company',
    'login_register'=>'Login/Register',
    'products'=>'Products',
    'i_agree_to_the'=>'I agree to the',
    'campaigns'=>'Campaigns',
    'account_information'=>'Account Information',
    'change_password'=>'Change Password',
    'terms_and_conditions'=>'Terms and Conditions',
    'how_it_works'=>'How It Works',
    'search_here'=>'Search Here',
    'all_campaigns'=>'All Campaigns',
    'watches'=>'Watches',
    'auto'=>'Auto',
    'coming_soon'=>'Coming Soon',
    'no_results_found'=>'No Results Found',
    'your_name'=>'Your Name',
    'your_email'=>'Your Email',
    'password'=>'Password',
    'confirm_password'=>'Confirm Password',
    'or'=>'Or',
    'sign_up'=>'Sign Up',
    'login_with'=>'Login With',
    'forget'=>'Forget',
    'phone'=>'Phone',
    'submit'=>'Submit',
    'your_email_or_phone'=>'Your Email Or Phone',
    'code'=>'Code',
    'new_password'=>'New Password',
    'confirm_your_password'=>'Confirm Your Password',
    'verify_your_account'=>'Verify Your Account',
    'all_rights_reserved'=>'All Rights Reserved',
    'cash'=>'Cash',
    'life_style'=>'Life Style',
    'electronics'=>'Electronics',
    'charities'=>'Charities',
    'customer_service'=>'Customer Service',
    'contact_us'=>'Contact Us',
    'faqs'=>'FAQs',
    'about_us'=>'About Us',
    'message'=>'Message',
    'step1'=>'Step 1',
    'step2'=>'Step 2',
    'step3'=>'Step 3',
    'step4'=>'Step 4',
    'log_out'=>'Logout',
    'full_name'=>'Full Name',
    //'l_name'=>'Logout',
    //'mobile'=>'Logout',
    'email'=>'Email',
    'address'=>'Address',
    'upload_new_image'=>'Upload New Image',
    'profile'=>'My Account',
    'my_orders'=>'My Orders',
    'cart'=>'Cart',
    'unit_price'=>'Per Unit',
    'buy_a'=>'Buy a',
    'add_to_cart'=>'Add To Cart',
    'out_of'=>'Out Of',
    'sold'=>'Sold',
    'prize_details'=>'Prize Details',
    'get_a_chance_to'=>'Get a chance to',
    'win'=>'Win',
    'aed'=>'AED',
    'kwd'=>'KWD',
    'sar'=>'SAR',
    'usd'=>'USD',
    'number'=>'Number',
    'total_quantity'=>'Total Quantity',
    'sold_quantity'=>'Sold Quantity',
    'price_per_unit'=>'Price Per Unit',
    'created_at'=>'Created At',
    'name'=>'Name',
    'image'=>'Image',
    'pay_now'=>'Pay Now',
    'united_arab_emirates_dirham'=>'United Arab Emirates Dirham',
    'kuwaiti_dinar'=>'Kuwaiti Dinar',
    'saudi_riyal'=>'Saudi Riyal',
    'united_states_dollar'=>'United States Dollar',
    'item_total'=>'Item Total',
    'campaigns_closing_soon'=>'CAMPAIGNS CLOSING SOON',
    'frequently_asked_questions'=>'Frequently Asked Questions',
    'kanz_app'=>'Kanz App',
    'product'=>'Product',
    'prize'=>'Prize',
    'ground_total'=>'Ground Total',
    'enter_promo_code'=>'Enter Promo Code',
    'total_products'=>'Total Products',
    'total_tickets'=>'Total Tickets',
    'apply_promo_code'=>'Apply To Promo Code',
    'exit'=>'Exit',
    'apply'=>'Apply',
    'orders'=>'Orders',
    'coupons'=>'Coupons',
    'order'=>'Order',
    'serial'=>'Serial Num',

    'wish_list'=>'Wish Lists',
    'our_payments_are_secured_with_state_of_the_art_three_dimensional_security_system_we_do_not_store_any_of_your_payment_or_credit_card_details_accepting_world_class_payment_providers'=>'Our payments are secured with state of the art three dimensional security system. We do
                                not store any of your payment or credit card details. Accepting world class payment
                                providers.',
    'prices_inclusive_of_VAT'=>'Prices inclusive of VAT',
    'donate_to_receive_an_additional_entry'=>'Donate to receive an additional entry!',
    'i_agree_to_donate_all_purchased_products_to_charity_as_per_the'=>'I agree to donate all purchased products to charity as per the ',
    'draw_terms_conditions'=>'Draw Terms & Conditions',

    'product_price_per_unit_including_all_taxes'=>'Product price per unit Including all taxes',














    'campaign_draw_terms_conditions'=>'Campaign Draw Terms & Conditions',
    'for_the_ultimate_shopping_experience_download_our_app'=>'For the ultimate shopping experience download our app',
    'enter_your_email_above_and_we_will_send_SMS_to_reset_your_password'=>'Enter Your Email Above And We Will Send SMS To Reset Your Password',




];


/*{{ trans('front.Home') }}*/
